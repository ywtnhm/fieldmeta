package org.coderfun.sys.dict.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.coderfun.common.OrderEntity_;

@Generated(value="Dali", date="2018-05-24T12:46:13.505+0800")
@StaticMetamodel(CodeClass.class)
public class CodeClass_ extends OrderEntity_ {
	public static volatile SingularAttribute<CodeClass, String> code;
	public static volatile SingularAttribute<CodeClass, String> name;
	public static volatile SingularAttribute<CodeClass, String> value;
	public static volatile SingularAttribute<CodeClass, String> moduleCode;
	public static volatile SingularAttribute<CodeClass, String> isSys;
}
