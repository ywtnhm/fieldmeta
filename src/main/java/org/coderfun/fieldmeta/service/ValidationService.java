package org.coderfun.fieldmeta.service;

import klg.j2ee.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.Validation;

public interface ValidationService extends BaseService<Validation, Long>{

}
