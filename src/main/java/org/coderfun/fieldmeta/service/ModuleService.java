package org.coderfun.fieldmeta.service;

import klg.j2ee.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.Module;

public interface ModuleService extends BaseService<Module, Long>{

}
