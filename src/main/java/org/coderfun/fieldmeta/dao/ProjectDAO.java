package org.coderfun.fieldmeta.dao;

import klg.j2ee.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Project;

public interface ProjectDAO extends BaseRepository<Project, Long> {

}
